package main

import (
	"fmt"
	"github.com/gorilla/websocket"
	"github.com/mitchellh/mapstructure"
	"net/http"
	"time"
)

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
	CheckOrigin:     func(r *http.Request) bool { return true },
}

type Message struct {
	Name string      `json:"name"`
	Data interface{} `json:"data"`
}

type Channel struct {
	Id   string `json:"id"`
	Name string `json:"name"`
}

func main() {
	http.HandleFunc("/", handler)
	http.ListenAndServe(":4000", nil)
}
func handler(writer http.ResponseWriter, request *http.Request) {
	socket, err := upgrader.Upgrade(writer, request, nil)
	if err != nil {
		fmt.Println(err)
		return
	}
	for {
		//msgType, msg, err := socket.ReadMessage()
		//if err != nil {
		//	fmt.Println(err)
		//	return
		//}
		var inMessage Message
		var outMessage Message

		if err := socket.ReadJSON(&inMessage); err != nil {
			fmt.Println(err)
			break
		}
		switch inMessage.Name {
		case "channel add":
			err := addChannel(inMessage.Data)
			if err != nil {
				outMessage = Message{"Error",err}
				if err := socket.WriteJSON(outMessage); err != nil{
					fmt.Println("error %#v",err)
					break
				}
			}
		case "channel subscribe":
			subscribeChannel(socket)
		}

		//fmt.Println(string(msg))
		//if err = socket.WriteMessage(msgType, msg); err != nil {
		//	fmt.Println(err)
		//	return
		//}
		fmt.Printf("%#v\n", inMessage)

	}
}
func addChannel(Data interface{}) (error) {
	var channel Channel
	err := mapstructure.Decode(Data, &channel)
	if err != nil {
		return err
	}
	channel.Id = "1"
	fmt.Printf("New Channel Added")
	fmt.Printf("%#v\n", channel)
	return nil
}

func subscribeChannel(socket *websocket.Conn){
	//TODO : rethinkDB Query / change feed
	for{
		time.Sleep(time.Second * 1)
		message := Message{"channel add", Channel{"1","Software Support"}}
		socket.WriteJSON(message)
		fmt.Println("Sent new Channel")
	}
}
